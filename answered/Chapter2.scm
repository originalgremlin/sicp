; utilities
(define nil '())

(define (parity x)
    (remainder x 2))

(define (gcd a b)
    (if (= b 0)
        a
        (gcd b (remainder a b))))

; 2.1
(define (make-rat n d)
    (let
        ((g (abs (gcd n d)))
        (n (if (< (* n d) 0) (- (abs n)) (abs n)))
        (d (abs d)))
    (cons (/ n g) (/ d g))))

(define (numer x) (car x))

(define (denom x) (cdr x))

(define (print-rat x)
    (newline)
    (display (numer x))
    (display "/")
    (display (denom x)))

; 2.2
(define (make-point x y) (cons x y))

(define (x-point p) (car p))

(define (y-point p) (cdr p))

(define (make-segment p1 p2) (cons p1 p2))

(define (start-segment s) (car s))

(define (end-segment s) (cdr s))

(define (midpoint-segment s)
    (let
        ((p1 (start-segment s))
        (p2 (end-segment s))
        (x1 (x-point p1))
        (y1 (y-point p1))
        (x2 (x-point p2))
        (y2 (y-point p2)))
    (make-point (/ (+ x1 x2) 2) (/ (+ y1 y2) 2))))

(define (print-point p)
    (newline)
    (display "(")
    (display (x-point p))
    (display ",")
    (display (y-point p))
    (display ")"))

(define p1 (make-point 2 5))
(define p2 (make-point 4 11))
(define s1 (make-segment p1 p2))
(define m1 (midpoint-segment s1))
(print-point m1)

; 2.3
(define (make-rect lower-left-point upper-right-point)
    (cons lower-left-point upper-right-point))

(define (lower-left-rect r)
    (car r))

(define (upper-right-rect r)
    (cdr r))

(define (width-rect r)
    (let
        ((left (x-point (lower-left-rect r)))
        (right (x-point (upper-right-rect r))))
    (abs (- right left))))

(define (height-rect r)
    (let
        ((bottom (y-point (lower-left-rect r)))
        (top (y-point (upper-right-rect r))))
    (abs (- top bottom))))

(define (area-rect r)
    (let
        ((w (width-rect r))
        (h (height-rect r)))
    (* w h)))

(define (perimeter-rect r)
    (let
        ((w (width-rect r))
        (h (height-rect r)))
    (+ w w h h)))

; 2.4
(define (xcons x y)
    (lambda (m) (m x y)))

(define (xcar z)
    (z (lambda (p q) p)))

(define (xcdr z)
    (z (lambda (p q) q)))

(xcar (xcons 10 20))
(xcdr (xcons 10 20))

; 2.5
(define (pow x n)
    (define (iter y count)
        (if (= count 0)
            y
            (iter (* x y) (- count 1))))
    (iter 1 n))

(define (xcons x y)
    (* (pow 2 x) (pow 3 y)))

(define (xcar z)
    (define (iter x a)
        (if (= (remainder x 2) 0)
            (iter (/ x 2) (+ a 1))
            a))
    (iter z 0))

(define (xcdr z)
    (define (iter x a)
        (if (= (remainder x 3) 0)
            (iter (/ x 3) (+ a 1))
            a))
    (iter z 0))

(xcar (xcons 10 20))
(xcdr (xcons 10 20))

; 2.6
(define zero
    (lambda (f) (lambda (x) x)))

(define (add-1 n)
    (lambda (f) (lambda (x) (f ((n f) x)))))

(define one
    (lambda (f) (lambda (x) (f x))))

(define two
    (lambda (f) (lambda (x) (f (f x)))))

; XXX: is this right?
(define (+ a b)
    (lambda (f) (lambda (x) (a (b x)))))

; 2.7 - 2.16
(define (make-interval a b)
    (cons a b))

(define (lower-bound x)
    (car x))

(define (upper-bound x)
    (cdr x))

(define (add-interval x y)
    (make-interval
        (+ (lower-bound x) (lower-bound y))
        (+ (upper-bound x) (upper-bound y))))

(define (sub-interval x y)
    (add-interval x (make-interval (- (upper-bound y)) (- (lower-bound y)))))

(define (mul-interval x y)
    (let ((p1 (* (lower-bound x) (lower-bound y)))
          (p2 (* (lower-bound x) (upper-bound y)))
          (p3 (* (upper-bound x) (lower-bound y)))
          (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4) (max p1 p2 p3 p4))))

(define (div-interval x y)
    (define (spans-zero i)
        (and (< (lower-bound i) 0) (> (upper-bound i) 0)))
    (if (spans-zero y)
        (error "Divisor interval spans 0.")
        (mul-interval x (make-interval (/ 1.0 (upper-bound y)) (/ 1.0 (lower-bound y))))))

(define (make-center-width c w)
    (make-interval (- c w) (+ c w)))

(define (center i)
    (/ (+ (lower-bound i) (upper-bound i)) 2))

(define (width i)
    (/ (- (upper-bound i) (lower-bound i)) 2))

(define (make-center-percent c p)
    (let (w (* c (/ p 100.0)))
    (make-center-width (- c w) (+ c w))))

(define (percent i)
    (* (/ (width i) (center i)) 100.0)

; 2.17
(define (last-pair items)
    (if (null? (cdr items))
        (car items)
        (last-pair (cdr items))))

; 2.18
(define (reverse items)
    (define (iter fwd rev)
        (if (null? fwd)
            rev
            (iter (cdr fwd) (cons (car fwd) rev))))
    (iter items ()))

; 2.19
(define us-coins (list 50 25 10 5 1))

(define uk-coins (list 100 50 20 10 5 2 1 0.5))

(define (cc amount coin-values)
    (define (no-more? items)
        (null? items))
    (define (first-denomination items)
        (car items))
    (define (except-first-denomination items)
        (cdr items))
    (cond
        ((= amount 0) 1)
        ((or (< amount 0) (no-more? coin-values)) 0)
        (else (+ (cc amount (except-first-denomination coin-values))
                 (cc (- amount (first-denomination coin-values)) coin-values)))))

; 2.20
; TODO: reverse the filtered list before returning it?
(define (same-parity . items)
    (define (iter par all filtered)
        (cond
            ((null? all) filtered)
            ((= par (parity (car all))) (iter par (cdr all) (cons (car all) filtered)))
            (else (iter par (cdr all) filtered))))
    (iter (parity (car items)) items nil))

(same-parity 1 2 3 4 5 6 7)
(same-parity 2 3 4 5 6 7)

; 2.21
(define (square-list items)
    (if (null? items)
        nil
        (cons (square (car items)) (square-list (cdr items)))))

(define (square-list-map items)
    (map square items))

; 2.22
(define (square-list-iter items)
    (define (iter things answer)
        (if (null? things)
            answer
            (iter (cdr things) (cons (square (car things)) answer))))
    (iter items nil))

(define (square-list-iter items)
    (define (iter things answer)
        (if (null? things)
            answer
            (iter (cdr things) (cons answer (square (car things))))))
    (iter items nil))

; 2.23
(define (for-each proc items)
    (define (iter rest)
        (cond
            ((null? rest)
                nil)
            (else (and
                (proc (car rest))
                (iter (cdr rest))))))
    (iter items))

(for-each (lambda (x) (newline) (display x)) (list 57 321 88))

; 2.25
(car (cdr (car (cdr (cdr (list 1 3 (list 5 7) 9))))))
(car (car (list (list 7))))
(car (cdr (car (cdr (car (cdr (car (cdr (car (cdr (car (cdr (list 1 (list 2 (list 3 (list 4 (list 5 (list 6 7))))))))))))))))))

; 2.27
(define (deep-reverse items)
    (define (iter fwd rev)
        (cond
            ((null? fwd) rev)
            ((pair? (car fwd)) (iter (cdr fwd) (cons (deep-reverse (car fwd)) rev)))
            (else (iter (cdr fwd) (cons (car fwd) rev)))))
    (iter items ()))

; 2.28
(define (count-leaves x)
    (cond
        ((null? x) 0)
        ((not (pair? x)) 1)
        (else (+ (count-leaves (car x)) (count-leaves (cdr x))))))

(define (fringe x)
    (define (recr items fringes)
        (cond
            ((null? items) fringes)
            ((not (pair? items)) (cons items fringes))
            (else (append fringes (recr (car items) fringes) (recr (cdr items) fringes)))))
    (recr x ()))

; 2.29
(define (make-mobile left right)
    (list left right))

(define (left-branch mobile)
    (car mobile))

(define (right-branch mobile)
    (car (cdr mobile)))

(define (make-branch length structure)
    (list length structure))

(define (branch-length branch)
    (car branch))

(define (branch-structure branch)
    (car (cdr branch)))

(define (branch-weight branch)
    (let
        ((structure (branch-structure branch)))
    (if (pair? structure)
        (+ (branch-weight (left-branch structure)) (branch-weight (right-branch structure)))
        structure)))

(define (total-weight mobile)
    (+ (branch-weight (left-branch mobile)) (branch-weight (right-branch mobile)))

(define (mobile-balanced? mobile)
    (let
        ((lb (left-branch mobile))
        (rb (right-branch mobile)))
    (and
        (= (* (total-weight lb) (branch-length lb)) (* (total-weight rb) (branch-length rb)))
        (mobile-balanced? lb)
        (mobile-balanced? rb))))

; 2.30
(define (square-tree tree)
    (cond
        ((null? tree) nil)
        ((not (pair? tree)) (square tree))
        (else (cons (square-tree (car tree)) (square-tree (cdr tree))))))

(define (square-tree tree)
    (map
        (lambda (sub-tree)
            (if (pair? sub-tree)
                (square-tree sub-tree)
                (square sub-tree)))
        tree))

(square-tree (list 1(list 2 (list 3 4) 5) (list 6 7)))

; 2.31
(define (tree-map proc tree)
    (map
        (lambda (sub-tree)
            (if (pair? sub-tree)
                (tree-map proc sub-tree)
                (proc sub-tree)))
        tree))

(define (square-tree tree) (tree-map square tree))

; 2.32
(define (subsets s)
    (if (null? s)
        (list nil)
        (let
            ((rest (subsets (cdr s))))
            (append rest (map (lambda (x) (cons (car s) x)) rest)))))

; 2.33
(define (accumulate op initial sequence)
    (if (null? sequence)
        initial
        (op (car sequence) (accumulate op initial (cdr sequence)))))

(define (map p sequence)
    (accumulate (lambda (x y) (cons (p x) y)) nil sequence))

(define (append seq1 seq2)
    (accumulate cons seq2 seq1))

(define (length sequence)
    (accumulate
        (lambda (x y) (+ 1 y))
        0
        sequence))

; 2.34
(define (horner-eval x coefficient-sequence)
    (accumulate
        (lambda (this-coeff higher-terms) (+ this-coeff (* x higher-terms)))
        0
        coefficient-sequence))

(horner-eval 2 (list 1 3 0 5 0 1))

; 2.35
(define (count-leaves-acc t)
    (accumulate
        (lambda (x y) (+ x y))
        0
        (map (lambda (st) (if (pair? st) (count-leaves-acc st) 1)) t)))

(count-leaves-acc '(1 (4 (9 16) 25) (36 49)))

; 2.36
(define (accumulate-n op init seqs)
    (if (null? (car seqs))
        nil
        (cons
            (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))

; 2.37
(define (dot-product v w)
    (accumulate + 0 (map * v w)))

(define (matrix-*-vector m v)
    (map (lambda (mi) (dot-product mi v)) m))

(define (transpose mat)
    (accumulate-n cons nil mat))

(define (matrix-*-matrix m n)
    (let
        ((cols (transpose n)))
    (map (lambda (mi) (matrix-*-vector cols mi)) m)))

(define m1 (list (list 1 2 3) (list 3 5 1) (list 1 1 1)))
(define v1 (list 1 2 3))
(matrix-*-vector m1 v1)
;(14 16 6)

(define m2 (list (list 1 2 3) (list 4 5 6) (list 7 8 9)))
(transpose m2)
;((1 4 7) (2 5 8) (3 6 9))

(matrix-*-matrix m1 m2)
;((30 36 42) (30 39 48) (12 15 18))

; 2.38
(define fold-right accumulate)

(define (fold-left op initial sequence)
    (define (iter result rest)
        (if (null? rest)
            result
            (iter (op result (car rest)) (cdr rest))))
    (iter initial sequence))

; 2.39
(define (reverse-right sequence)
    (fold-right
        (lambda (x y) (append y (list x)))
        nil
        sequence))

(define (reverse-left sequence)
    (fold-left
        (lambda (x y) (cons y x))
        nil
        sequence))

; 2.40
(define (flatmap proc seq)
    (accumulate append nil (map proc seq)))

(define (enumerate-interval low high)
    (if (> low high)
        nil
        (cons low (enumerate-interval (+ low 1) high))))

(define (unique-pairs n)
    (flatmap
        (lambda (i)
            (map
                (lambda (j) (list i j))
                (enumerate-interval 1 (- i 1))))
        (enumerate-interval 1 n)))

(define (smallest-divisor n)
    (define (next n)
        (if (= n 2) 3 (+ n 2)))
    (define (divides? a b)
        (= (remainder b a) 0))
    (define (find-divisor n test-divisor)
        (cond
            ((> (square test-divisor) n) n)
            ((divides? test-divisor n) test-divisor)
            (else (find-divisor n (next test-divisor)))))
    (find-divisor n 2))

(define (prime? n)
    (= n (smallest-divisor n)))

(define (prime-sum-pairs n)
    (define (make-pair-sum pair)
        (list (car pair) (cadr pair) (+ (car pair) (cadr pair))))
    (define (prime-sum? pair)
        (prime? (+ (car pair) (cadr pair))))
    (map make-pair-sum (filter prime-sum? (unique-pairs n))))

; 2.41
(define (unique-triples n)
    (flatmap
        (lambda (i)
            (flatmap
                (lambda (j)
                    (map
                        (lambda (k) (list i j k))
                        (enumerate-interval 1 (- j 1))))
                (enumerate-interval 1 (- i 1))))
        (enumerate-interval 1 n)))

(define (triple-sum n s)
    (define (sum-equals? s triple)
        (= s (+ (car triple) (cadr triple) (caddr triple))))
    (filter
        (lambda (i) (sum-equals? s i))
        (unique-triples n)))

; 2.42
(define (queens board-size)
    (define (adjoin-position new-row k rest-of-queens)
        (map (lambda (row) (cons new-row row)) rest-of-queens))

    (define empty-board (map (lambda (x) nil) (enumerate-interval 1 board-size)))

    (define (safe? k positions)
        (= 1 1))

    (define (queen-cols k)
        (if (= k 0)
            (list empty-board)
            (filter
                (lambda (positions) (safe? k positions))
                (flatmap
                    (lambda (rest-of-queens)
                        (map
                            (lambda (new-row) (adjoin-position new-row k rest-of-queens))
                            (enumerate-interval 1 board-size)))
                    (queen-cols (- k 1))))))
    (queen-cols board-size))
