; utility functions
(define (even? n)
    (= (remainder n 2) 0))

(define (double n)
    (* n 2))

(define (halve n)
    (/ n 2.0))

(define (average a b)
    (/ (+ a b) 2.0))

(define (square n)
    (* n n))

(define (cube n)
    (* n n n))

(define (gcd a b)
    (if (= b 0)
        a
        (gcd b (remainder a b))))

(define (relative-prime? a b)
    (= (gcd a b) 1))

(define (average-damp f)
    (lambda (x) (average x (f x))))

(define (identity n)
    n)

(define (inc n)
    (+ n 1))

(define (pow x n)
    (define (iter y count)
        (if (= count 0)
            y
            (iter (* x y) (- count 1))))
    (iter 1 n))

; sum of squares
(define (sum-of-squares x y)
    (+ (square x) (square y)))

(define (sum-of-largest x y z)
    (cond
        ((and (<= x y) (<= x z)) (sum-of-squares y z))
        ((and (<= y x) (<= y z)) (sum-of-squares x z))
        (else (sum-of-squares x y))))

; Newton's method for square roots
(define (sqrt x)
    (define (sqrt-iter old-guess x)
        (define new-guess (improve old-guess x))
        (if (good-enough? new-guess old-guess)
            new-guess
            (sqrt-iter new-guess x)))

    (define (improve guess x)
        (average guess (/ x guess)))

    (define (average x y)
        (/ (+ x y) 2))

    (define (good-enough? new-guess old-guess)
        (< (abs (- old-guess new-guess)) (/ new-guess 1e6)))

    (sqrt-iter 1.0 x))

; Newton's method for cube roots
(define (cbrt x)
    (define (cbrt-iter old-guess x)
        (define new-guess (improve-cbrt old-guess x))
        (if (good-enough? new-guess old-guess)
            new-guess
            (cbrt-iter new-guess x)))

    (define (improve-cbrt guess x)
        (/ (+ (/ x (square guess)) (* 2 guess)) 3))

    (define (good-enough? new-guess old-guess)
        (< (abs (- old-guess new-guess)) (/ new-guess 1e6)))

    (cbrt-iter 1.0 x))

; Ackerman's function
(define (A x y)
    (cond
        ((= y 0) 0)
        ((= x 0) (* 2 y)) ((= y 1) 2)
        (else (A (- x 1) (A x (- y 1))))))

; 1.11
; A function f is defined by the rule that
;   f(n) = n if n < 3 and
;   f(n) = f(n - 1) + 2f(n - 2) + 3f(n - 3) if n >= 3.
; Write a procedure that computes f by means of a recursive process.
(define (fr n)
    (define (f-rec n)
        (if (< n 3)
            n
            (+ (f-rec (- n 1)) (* 2 (f-rec (- n 2))) (* 3 (f-rec (- n 3))))))
    (f-rec n))

; Write a procedure that computes f by means of an iterative process.
(define (fi n)
    (define (f-iter a b c count)
        (if (= count 0)
            c
            (f-iter (+ a (* 2 b) (* 3 c)) a b (- count 1))))
    (f-iter 2 1 0 n))

; Pascal's triangle
(define (pascal row col)
    (cond
        ((<= col 1) 1)
        ((>= col row) 1)
        (else (+ (pascal (- row 1) (- col 1)) (pascal (- row 1) col)))))

; Exercise 1.16.
; Design a procedure that evolves an iterative exponentiation process that uses successive squaring
; and uses a logarithmic number of steps, as does fast-expt.
(define (fast-expt b n)
    (define (even? n)
        (= (remainder n 2) 0))
    (define (fast-expt-iter a b n)
        (cond
            ((= n 0) a)
            ((even? n) (fast-expt-iter a (* b b) (/ n 2)))
            (else (fast-expt-iter (* a b) b (- n 1)))))
    (fast-expt-iter 1 b n))


; 1.17
(define (fast-mult a b)
    (define (even? n)
        (= (remainder n 2) 0))
    (define (double n)
        (* n 2))
    (define (halve n)
        (/ n 2))
    (cond
        ((= b 0) 0)
        ((= b 1) a)
        ((even? b) (double (fast-mult a (halve b))))
        (else (+ a (fast-mult a (- b 1))))))

; 1.18
(define (fast-mult a b)
    (define (even? n)
        (= (remainder n 2) 0))
    (define (double n)
        (* n 2))
    (define (halve n)
        (/ n 2))
    (define (fast-mult-iter rv a b)
        (cond
            ((= b 0) rv)
            ((even? b) (fast-mult-iter rv (double a) (halve b)))
            (else (fast-mult-iter (+ rv a) a (- b 1)))))
    (fast-mult-iter 0 a b))

; 1.19
(define (fib n)
    (define (fib-iter a b p q count)
        (cond
            ((= count 0) b)
            ((even? count) (fib-iter
                a
                b
                (+ (* p p) (* q q))
                (+ (* 2 p q) (* q q))
                (/ count 2)
            ))
            (else (fib-iter
                (+ (* b q) (* a q) (* a p))
                (+ (* b p) (* a q))
                p
                q
                (- count 1)
            ))
        ))
    (fib-iter 1 0 0 1 n))

; 1.21
(define (smallest-divisor n)
    (define (next n)
        (if (= n 2) 3 (+ n 2)))
    (define (divides? a b)
        (= (remainder b a) 0))
    (define (find-divisor n test-divisor)
        (cond
            ((> (square test-divisor) n) n)
            ((divides? test-divisor n) test-divisor)
            (else (find-divisor n (next test-divisor)))))
    (find-divisor n 2))


(define (prime? n)
    (= n (smallest-divisor n)))

; 1.22
(define (timed-prime-test n)
    (define (report-prime elapsed-time)
        (newline)
        (display n)
        (display " *** ")
        (display elapsed-time))
    (define (start-prime-test n start-time)
        (if (fast-prime? n 1000)
            (report-prime (- (runtime) start-time))))
    (start-prime-test n (runtime)))

(define (search-for-primes a b)
    (define (even? n)
        (= (remainder n 2) 0))
    (define c (if (even? a) (+ a 1) a))
    (timed-prime-test c)
    (if (<= c b)
        (search-for-primes (+ c 2) b)))

; 1.24
(define (fast-prime? n times)
    (define (even? n)
        (= (remainder n 2) 0))
    (define (expmod base exp m)
        (cond
            ((= exp 0) 1)
            ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
            (else (remainder (* base (expmod base (- exp 1) m)) m))))
    (define (fermat-test n)
        (define (try-it a)
            (= (expmod a n n) a))
        (try-it (+ 1 (random (- n 1)))))
    (cond
        ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))

; 1.30
(define (sum term a next b)
    (define (iter a result)
        (if (> a b)
            result
            (iter (next a) (+ result (term a)))))
    (iter a 0))

; 1.31
(define (product term a next b)
    (define (iter a result)
        (if (> a b)
            result
            (iter (next a) (* result (term a)))))
    (iter a 1))

(define (fact n)
    (product identity 1 inc n))

(define (product-r term a next b)
    (if (> a b)
        1
        (* (term a) (product-r term (next a) next b))))

(define (fact-r n)
    (product-r identity 1 inc n))

(define (pi-product n)
    (define (pi-term n)
        (if (even? n)
            (/ (+ n 2.0) (+ n 1.0))
            (/ (+ n 1.0) (+ n 2.0))))
    (* 4 (product pi-term 1 inc n)))

; 1.32
(define (accumulate combiner null-value term a next b)
    (define (iter a result)
        (if (> a b)
            result
            (iter (next a) (combiner result (term a)))))
    (iter a null-value))

(define (accumulate-r combiner null-value term a next b)
    (if (> a b)
        null-value
        (combiner (term a) (accumulate-r term (next a) next b))))

(define (sum term a next b)
    (accumulate + 0 term a next b))

(define (product term a next b)
    (accumulate * 1 term a next b))

; 1.33
(define (filtered-accumulate filter? combiner null-value term a next b)
    (define (iter a result)
        (cond
            ((> a b) result)
            ((filter? a) (iter (next a) (combiner result (term a))))
            (else (iter (next a) result))))
    (iter a null-value))

(define (sum-of-prime-squares a b)
    (filtered-accumulate prime? + 0 square a inc b))

(define (product-of-relative-primes n)
    (filtered-accumulate relative-prime? * 1 identity 1 inc (- n 1)))

; 1.35
(define tolerance 0.00001)
(define (fixed-point f first-guess)
    (define (close-enough? v1 v2)
        (< (abs (- v1 v2)) tolerance))
    (define (try guess)
        (let
            ((next (f guess)))
            (if (close-enough? guess next)
                next
                (try next))))
    (try first-guess))

(define (golden-ratio)
    (fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.0))

; 1.36
(define tolerance 0.00001)
(define (fixed-point f first-guess)
    (define (close-enough? v1 v2)
        (< (abs (- v1 v2)) tolerance))
    (define (try guess)
        (let
            ((next (f guess)))
            (display guess)
            (newline)
            (if (close-enough? guess next)
                next
                (try next))))
    (try first-guess))

(define (x-to-the-x n)
    (fixed-point-display (lambda (x) (average x (/ (log n) (log x)))) 2.0))

; 1.37
(define (cont-frac n d k)
    (define (iter result n d k)
        (if (= k 0)
            result
            (iter (+ (d (- k 1)) (/ (n k) result)) n d (- k 1))))
    (iter (d k) n d k))

(define (cont-frac-r n d k)
    (define (recr count)
        (if (> count k)
            0
            (/ (n count) (+ (d count) (recr (+ count 1))))))
    (/ 1 (recr 1)))

(define (golden-ratio-cf n)
    (cont-frac (lambda (i) 1.0) (lambda (i) 1.0) n))

; 1.38
(define (e-cf n)
    (+ 1 (cont-frac
        (lambda (i) 1.0)
        (lambda (i)
            (if (= (remainder i 3) 2)
                (* (/ (+ i 1) 3) 2)
                1))
        n)))

; 1.39
(define (tan-cf x n)
    (+ 1 (cont-frac
        (lambda (i) (if (= i 1) x (* x (- x))))
        (lambda (i) (- (* i 2) 1.0))
        n)))

; 1.40
(define dx 0.00001)
(define (deriv g)
    (lambda (x) (/ (- (g (+ x dx)) (g x)) dx)))

(define (newton-transform g)
    (lambda (x) (- x (/ (g x) ((deriv g) x)))))

(define (newtons-method g guess)
    (fixed-point (newton-transform g) guess))

(define (cubic a b c)
    (lambda (x) (+ (* x x x) (* a x x) (* b x) c)))

; 1.41
(define (double f)
    (lambda (x) (f (f x))))

(define (dinc x)
    ((double inc) x))

(((double (double (double double))) inc) 5)

; 1.42
(define (compose f g)
    (lambda (x) (f (g x))))

; 1.43
(define (repeated f n)
    (define (iter ff nn)
        (if (= nn 1)
            ff
            (iter (compose ff f) (- nn 1))))
    (iter f n))

; 1.44
(define dx 0.00001)
(define (smooth f)
    (lambda (x) (/ (+ (f (- x dx)) (f x) (f (- x dx))) 3)))

(define (n-fold-smooth f n)
    (repeated (smooth f) n))

; 1.45
; I think this is the right procedure but I don't know how many times to repeat the average damping.
; So far I've tried n, (- n 1), (floor (/ n 2)), (ceiling (/ n 2 )), and (ceiling (log n))
(define (nth-root-fp x n)
    (fixed-point
        (repeated (average-damp (lambda (y) (/ x (pow y (- n 1))))) (ceiling (log n)))
        1.0))

; 1.46
(define (iterative-improve good-enough? improve)
    (define (iter old-guess x)
        (let ((new-guess (improve old-guess x)))
        (if (good-enough? new-guess old-guess)
            new-guess
            (iter new-guess x))))
    (lambda (guess x) (iter guess x)))

(define (sqrt-ii x)
    ((iterative-improve
        (lambda (new-guess old-guess) (< (abs (- old-guess new-guess)) (/ new-guess 1e6)))
        (lambda (guess x) (/ (+ guess (/ x guess)) 2)))
        1.0
        x))

(define (fixed-point-ii f first-guess)
    ((iterative-improve
        (lambda (new-guess old-guess) (< (abs (- old-guess new-guess)) 1e-5))
        (lambda (guess x) (f guess)))
        first-guess
        first-guess))

(define (golden-ratio-ii)
    (fixed-point-ii (lambda (x) (+ 1 (/ 1 x))) 1.0))
